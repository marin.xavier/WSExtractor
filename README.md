# WarpScript documentation extractor

Allow to extract Warpscript doc from .md files and generate datas for VSCode langage server.

## Usage

    npm install
    node index.js /path/to/www.warp10.io/04_WarpScript_Reference
