let walk = require('walk')
let fs = require('fs')
let files = []
let descContent = ''
let rex = []
let i = 1
var re = new RegExp('desc: "(.*)"')
var title = new RegExp('title: "(.*)"')
var fn = new RegExp('layout: .?function.?')
var ope = /^[^\s\*\+\=\|~|&|\||/|\[|\]|\{|\}]+$/
var operand = new RegExp('^[^=+*|(>|<){}\\[\\]]+$')
// Walker options
let walker = walk.walk(process.argv[2], { followLinks: false })
String.prototype.isUpperCase = function () {
  return this.valueOf().toUpperCase() === this.valueOf()
}
String.prototype.replaceAll = function (search, replacement) {
  var target = this
  return target.split(search).join(replacement)
}

walker.on('file', function (root, stat, next) {
  // Add this file to the list of files
  let functionName = stat.name.replace('.md', '').replace('function_', '')
  var contents = fs.readFileSync(root + '/' + stat.name , 'utf8')
  var r = contents.match(re)
  var t = contents.match(title)
  if (t) {
    functionName = t[1]
  }
  functionName = functionName.replaceAll('\.', '\\\\.')
  if (contents.match(fn) && r && functionName.match(ope)) { // && !functionName.match(operand)) { //functionName !== '[]' && functionName !== '{}' && functionName !== '**' && functionName !== '|' && functionName !== '||') {
    let desc = r[1]   
    rex.push(functionName)
    if (i !== 1) {
      descContent += ' else '
    }
    descContent += `if (item.data === ${i}) {
      item.detail = '${functionName}',
        item.documentation = '${desc.replaceAll('\'', '\\\'')}'
    }`

    files.push({
      label: functionName,
      kind: 'CompletionItemKind.Text',
      data: i
    })
    i++
  }
  next()
})

walker.on('end', function () {
  fs.writeFile('./functions.js', 
  JSON.stringify(files)
  .replaceAll('"CompletionItemKind.Text"', 'CompletionItemKind.Text')
  .replaceAll('"label"', 'label')
  .replaceAll('"kind"', 'kind')
  .replaceAll('"data"', 'data')
  .replaceAll(',', ',\n')
  .replaceAll('"', '\''), function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('The functions was saved!')
  })

  console.log(i)
  fs.writeFile('./rex.js', rex.join('|'), function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('The rex was saved!')
  })
  fs.writeFile('./desc.js', descContent, function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('The desc was saved!')
  })
})
